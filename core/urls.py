from django.urls import path

from core.views import HomePage, Info

urlpatterns = [
    path('', HomePage.as_view(), name='homepage'),
    path('info/', Info.as_view(), name='info')
]
