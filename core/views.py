from django.shortcuts import render

from django.shortcuts import render
from django.views import View


class HomePage(View):
    def get(self, request):
        context = {'title': 'сеть Кинотеатро "Ultra"'}
        return render(request=request, template_name='core/index.html', context=context)


class Info(View):
    def get(self, request):
        context = {'title': 'Информация о проекте'}
        return render(request=request, template_name='core/info.html', context=context)
