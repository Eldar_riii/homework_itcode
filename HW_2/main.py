class Counter():
    """Класс счетчик для положительных чисел"""
    def __init__(self, initial_value):
        self.value = initial_value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):
    """Класс счетчик для отрицательных чисел"""

    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(number):
    if number >= 0:
        counter = Counter(number)
    else:
        counter = ReverseCounter(number)
    return counter


if __name__ == "__main__":
    counter = get_counter(5)
    a = counter.inc()
    b = counter.inc()
    c = counter.inc()
    d = counter.dec()

    print(a, b, c, d)

    counter = get_counter(-1)
    a = counter.inc()
    b = counter.inc()
    c = counter.inc()
    d = counter.dec()

    print(a, b, c, d)
