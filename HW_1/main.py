def format_to_date(day, month, year):
    """Эта функция форматирует дату в строчный вид"""
    months = ["января", "февраля", "марта", "апреля", "мая", "июня",
             "июля", "августа", "сентября", "октября", "ноября", "декабря"]

    """Проверям весокосный год или нет"""
    if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        days_in_months = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    else:
        days_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    """Проверям на корректность входные данные"""
    if month > 12 or day > days_in_months[month - 1] or year < 0:
        str_date = "Неверный формат"
    else:
        str_date = f"{day} {months[month - 1]} {year} года"
    return str_date


def count_names(names_tuple):
    """Эта функция считает сколько раз имя повторяется в кортеже"""
    names_count = {}
    for name in names_tuple:
        if name in names_count:
            names_count[name] += 1
        else:
            names_count[name] = 1
    return names_count


def format_person_data(person_data):
    """Эта фунцкия форматирует данные человека в строчный вид"""
    first_name = person_data.get("first_name", "")
    last_name = person_data.get("last_name", "")
    middle_name = person_data.get("middle_name", "")
    str_data = ""
    if not first_name and not last_name:
        str_data = f"Нет данных"
    elif not first_name:
        str_data = f"{last_name}"
    else:
        str_data = f"{last_name} {first_name} {middle_name}"
    return str_data.strip()


def is_prime(number):
    """Эта функция проверяет является ли число простым"""
    if number < 2:
        return False
    else:
        for i in range(2, int(number**0.5) + 1):
            if number % i == 0:
                return False
    return True


def get_unique_numbers(*args):
    """Эта функция находит уникальные числа среди аргументов"""
    unique_num = []
    for arg in args:
        if isinstance(arg, (int, float)) and not isinstance(arg, (bool)):
            if arg not in unique_num:
                unique_num.append(arg)
    unique_num.sort()
    return unique_num


