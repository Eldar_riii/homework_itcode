import unittest
from main import *

class FunctionTest(unittest.TestCase):
    def test_format_to_date(self):
        self.assertEqual(format_to_date(12, 10, 2003), "12 октября 2003 года")
        self.assertEqual(format_to_date(29, 2, 2020), "29 февраля 2020 года")
        self.assertEqual(format_to_date(29, 2, 1900), "Неверный формат")
        self.assertEqual(format_to_date(29, 2, 2000), "29 февраля 2000 года")
        self.assertEqual(format_to_date(32, 4, 2001), "Неверный формат")
        self.assertEqual(format_to_date(3, 13, 2001), "Неверный формат")
        self.assertEqual(format_to_date(3, 10, -2001), "Неверный формат")

    def test_count_names(self):
        self.assertEqual(count_names(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')),
                         {'Олег': 1, 'Игорь': 2, 'Анна': 3, 'Василий': 1})

    def test_format_person_data(self):
        self.assertEqual(format_person_data({'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': 'Иванович'}), "Иванов Иван Иванович")
        self.assertEqual(format_person_data({'first_name': 'Иван', 'last_name': '', 'middle_name': 'Иванович'}), "Иван Иванович")
        self.assertEqual(format_person_data({'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': ''}), "Иванов Иван")
        self.assertEqual(format_person_data({'first_name': '', 'last_name': 'Иванов', 'middle_name': 'Иванович'}), "Иванов")
        self.assertEqual(format_person_data({'first_name': '', 'last_name': '', 'middle_name': 'Иванович'}), "Нет данных")
        self.assertEqual(format_person_data({'first_name': '', 'last_name': '', 'middle_name': ''}), "Нет данных")

    def test_is_prime(self):
        self.assertEqual(is_prime(-3), False)
        self.assertEqual(is_prime(0), False)
        self.assertEqual(is_prime(1), False)
        self.assertEqual(is_prime(2), True)
        self.assertEqual(is_prime(13), True)
        self.assertEqual(is_prime(16), False)

    def test_get_unique_numbers(self):
        self.assertEqual(get_unique_numbers(1, '2', 'text', 42, None, None, None, 15, True, 1, 1), [1, 15, 42])


if __name__ == "__main__":
    unittest.main()